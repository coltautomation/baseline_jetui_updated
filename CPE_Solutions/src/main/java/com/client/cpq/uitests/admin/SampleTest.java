package com.client.cpq.uitests.admin;
import java.io.IOException;

public class SampleTest {
	
	 private String name; // private = restricted access

	  // Getter
	  public String getName() {
	    return name;
	  }

	  // Setter
	  public void setName(String newName) {
	    this.name = newName;
	  }

	public static void main(String[] args) throws IOException, InterruptedException {
			
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String UI_Type = "TS_03_Medium_Scenario";
		int x = 6, y = 4;
		
		SampleTest myObj = new SampleTest();
	    myObj.setName("John"); // Set the value of the name variable to "John"
	    System.out.println(myObj.getName());
		
//		Addition
	    System.out.println("Addition "+ (x + y));
	    
//		Subtraction
	    System.out.println("Subtraction "+ (x - y));
	    
//		Multiplication
	    System.out.println("Multiplication "+ (x * y));
	    
//		Division
	    System.out.println("Division "+ (x / y));
	    
//		Modulus
	    System.out.println("Modulus "+ (x % y));
	    
//		Increment
	    System.out.println("Increment "+ (++x));
	    
//		Decrement
	    System.out.println("Decrement "+ (--x));
	    
	    
//	    String Operations
	    
//	    Convert Case
	    String txt = "My Programme";
	    System.out.println(txt.toUpperCase());   
	    System.out.println(txt.toLowerCase());
	    
//	    String Index
	    System.out.println(txt.indexOf("Programme"));
	    
//	    String Concatenate
	    String firstName = "test";
	    String lastName = "Automation";
	    System.out.println(firstName + " " + lastName);
//	    alternate
	    System.out.println(firstName.concat(lastName));
	    
//	    To include double quotes in string
	    System.out.println("welcome to \"Selenium\" automation");
	    
//	    To Insert a New line
	    System.out.println("Test Automation \n Training");
	    
//	    If condition
	    int x1 = 20;
	    int y1 = 18;
	    if (x1 > y1) {
	      System.out.println("x is greater than y");
	    }
	    
//	    Switch
	    switch("Selenium") {
	    case "Java":
	    	System.out.println("case Entered in to Java Block");
	    	break;
	    case "Python":
	    	System.out.println("case Entered in to Python Block");
	    	break;
	    case "Selenium":
	    	System.out.println("case Entered in to Selenium Block");
	    	break;
	    }
	      
//	      while Loop
	      int i = 0;
	      while (i < 5) {
	        System.out.println(i);
	        i++;
	      }
	      
//	      for Loop
	      for (i = 0; i < 5; i++) {
	    	  System.out.println(i);
	    	}
	      
//	      Break and Continue
	      for (i = 0; i < 10; i++) {
	    	  if (i == 4) {
	    	    break;
	    	  }
	    	  System.out.println(i);
	    	}
	      
//	      Initializing Array
//	      String[] lapbrands;
	      String[] lapbrands = {"Dell", "HP", "MSI", "Sony"};
	      
//	      Array Length
	      System.out.println(lapbrands.length);
	      
	      System.out.println(lapbrands[0]);
	      
	      lapbrands[1] = "Acer";
	      System.out.println(lapbrands[1]);
	      
//	      To Print Value one by one
//	      Type 1
	      for (i = 0; i < lapbrands.length; i++) {
	        System.out.println(lapbrands[i]);
	      }
	      
//	      Type 2
	      for (String iCurrent : lapbrands) {
	    	  System.out.println(iCurrent);
	    	}
		
	}

}
